# home19
Теоретичні питання
1. Які способи JavaScript можна використовувати для створення та додавання нових DOM-елементів?
document.createElement()
innerHTML
insertAdjacentHTML
appendChild
cloneNode
insertBefore

2. Опишіть покроково процес видалення одного елементу (умовно клас "navigation") зі сторінки.
const elementToRemove = document.querySelector('.navigation');
elementToRemove.remove();

3. Які є методи для вставки DOM-елементів перед/після іншого DOM-елемента?
insertBefore
insertAdjacentElement(beforebegin, afterbegin, beforeend, afterend)
insertAdjacentHTML
insertAdjacentText