// 1. Створіть новий елемент <a>, задайте йому текст "Learn More" і атрибут href з посиланням на "#". Додайте цей елемент в footer після параграфу.

// 2. Створіть новий елемент <select>. Задайте йому ідентифікатор "rating", і додайте його в тег main перед секцією "Features".
// Створіть новий елемент <option> зі значенням "4" і текстом "4 Stars", і додайте його до списку вибору рейтингу.
// Створіть новий елемент <option> зі значенням "3" і текстом "3 Stars", і додайте його до списку вибору рейтингу.
// Створіть новий елемент <option> зі значенням "2" і текстом "2 Stars", і додайте його до списку вибору рейтингу.
// Створіть новий елемент <option> зі значенням "1" і текстом "1 Star", і додайте його до списку вибору рейтингу.

//1
const a = document.createElement('a');
a.innerText = "Learn More";
a.setAttribute('href', '#');
const footer = document.querySelector('footer');
footer.insertAdjacentElement('beforeend', a);
//2

const select = document.createElement('select');
select.id = 'rating';
const main = document.querySelector('main');
const features = main.querySelector('.features');
main.insertBefore(select, features);


for (let index = 4; index > 0; index--) {
  let option = document.createElement('option');
  option.value = `${index}`;
  if(index === 1) {
    option.innerText = `${index} Star`;
  } else {
    option.innerText = `${index} Stars`;
  }
  select.insertAdjacentElement("afterbegin", option);
}

console.log(main);

